import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { AlertController, Platform} from 'ionic-angular';
import { BatteryStatus } from 'ionic-native';

declare var batteryLevel: any;
declare var navigator: any;
declare var Connection: any;



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  hideTopTab:boolean=true;
   x(){
 console.log(this.hideTopTab);

    this.hideTopTab = !this.hideTopTab;
}

// hideBottomTab:boolean=true;
//    y(){
//  console.log(this.hideBottomTab);

//     this.hideBottomTab = !this.hideBottomTab;
// }
status:any;
  constructor(public alert:AlertController , public navCtrl: NavController, public platform: Platform) {

this.platform.ready().then(()=>{
  let subscription = BatteryStatus.onChange().subscribe( (status) => { 
    console.log(status.level, status.isPlugged); 
    this.status=status.level;
    } );
   } );

  }

  onBatteryStatus(){
  this.platform.ready().then(()=>{
  let subscription = BatteryStatus.onChange().subscribe( (status) => { 
     alert('battery status: '+status.level+' isPlugged: '+status.isPlugged);
    this.status=status.level;
    } );
   } );
}

checkNetwork() {
        this.platform.ready().then(() => {
            var networkState = navigator.connection.type;
            var states = {};
            states[Connection.UNKNOWN]  = 'Unknown connection';
            states[Connection.ETHERNET] = 'Ethernet connection';
            states[Connection.WIFI]     = 'WiFi connection';
            states[Connection.CELL_2G]  = 'Cell 2G connection';
            states[Connection.CELL_3G]  = 'Cell 3G connection';
            states[Connection.CELL_4G]  = 'Cell 4G connection';
            states[Connection.CELL]     = 'Cell generic connection';
            states[Connection.NONE]     = 'No network connection';
            let alert = this.alert.create({
                title: "Connection Status",
                subTitle: states[networkState],
                buttons: ["OK"]
            });
            alert.present();
        });
    }
}

// saikrishna garre